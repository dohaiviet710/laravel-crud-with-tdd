<?php

namespace Tests\Feature;

use App\Models\Tasks;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetListTaskTest extends TestCase
{
    /** @test */
    public function user_can_get_all_task(): void
    {
        $task = Tasks::factory()->create();

        $response = $this->get($this->GetListTestRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('tasks.index');
        $response->assertSee($task->name);
    }

    public function GetListTestRoute()
    {
        return route('tasks.index');
    }



}
