<?php

namespace Tests\Feature;

use App\Models\Tasks;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class ShowTaskTest extends TestCase
{
    /** @test */
    public function unauthenticate_user_can_not_show_task(): void
    {
        $task = Tasks::factory()->create();
        $response = $this->get(route('tasks.show', $task->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function authenticate_user_can_show_task(): void
    {
        $this->actingAs(User::factory()->create());
        $task = Tasks::factory()->create();
        $response = $this->get(route('tasks.show', $task->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('tasks.show');
    }

    /** @test */
    public function authenticate_user_can_not_show_task_if_task_not_exist()
    {
        $this->actingAs(User::factory()->create());
        $taskId = -1;
        $response = $this->get(route('tasks.show', $taskId));
        $response->assertStatus(Response::HTTP_NOT_FOUND);

    }

}
