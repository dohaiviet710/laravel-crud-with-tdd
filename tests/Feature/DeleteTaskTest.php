<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\Tasks;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteTaskTest extends TestCase
{
    /** @test */
    public function unauthenticate_user_can_not_delete(): void
    {
        $task = Tasks::factory()->create();
        $response = $this->delete('/tasks/delete/' . $task->id);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function authenticate_user_can_delete_task(): void
    {
        $this->actingAs(User::factory()->create());
        $task = Tasks::factory()->create();
        $beforedelete = Tasks::count();
        $response = $this->delete('/tasks/delete/' . $task->id);
        $afterdelete = Tasks::count();
        $this->assertEquals($beforedelete-1,$afterdelete);
    }

    /** @test */
    public function authenticate_user_can_not_delete_if_task_not_exist(): void
    {
        $this->actingAs(User::factory()->create());
        $taskID = -1;
        $response = $this->delete('/tasks/delete/' . $taskID);
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }


}
