<?php

namespace Tests\Feature;

use App\Models\Tasks;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreateTaskTest extends TestCase
{
    public function CreateTaskRoute()
    {
        return route('tasks.store');
    }

    /** @test */
    public function unauthenticated_user_can_not_see_view_form_create_task(): void
    {
        $response = $this->get(route('tasks.create'));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function authenticated_user_can_create_new_task(): void
    {
        $this->actingAs(User::factory()->create());
        $beforeCreate = Tasks::count();
        $task = Tasks::factory()->make()->toArray();
        $response = $this->post($this->CreateTaskRoute(), $task);
        $afterCreate = Tasks::count();
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertEquals($beforeCreate + 1, $afterCreate);
    }

    /** @test */
    public function unauthenticated_user_can_not_create_new_task(): void
    {
        $task = Tasks::factory()->make()->toArray();
        $response = $this->post($this->CreateTaskRoute(), $task);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function authenticated_user_can_not_create_new_task_if_feild_name_null(): void
    {
        $this->actingAs(User::factory()->create());
        $task = Tasks::factory()->make(['name'=>null])->toArray();
        $response = $this->post($this->CreateTaskRoute(), $task);
        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function authenticated_user_can__see_view_form_create_task(): void
    {
        $this->actingAs(user::factory()->create());
        $response = $this->get(route('tasks.create'));
        $response->assertViewIs('tasks.create');
    }



}
