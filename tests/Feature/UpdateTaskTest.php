<?php

namespace Tests\Feature;

use App\Models\Tasks;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class UpdateTaskTest extends TestCase
{
    /** @test */
    public function unauthenticated_user_can_not_see_update_form(): void
    {
        $task = Tasks::factory()->create();
        $response = $this->get('tasks/edit/' . $task->id);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function authenticated_user_can_see_update_form(): void
    {
        $this->actingAs(User::factory()->create());
        $task = Tasks::factory()->create();
        $response = $this->get('tasks/edit/' . $task->id);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('tasks.edit');
    }

    /** @test */
    public function  authenticated_user_can_not_update_task_if_name_or_content_null()
    {
        $this->actingAs(User::factory()->create());
        $task = Tasks::factory()->create();
        $dataUpdate = Tasks::factory()->make(['name' => null, 'content' => null])->toArray();
        $response = $this->put('tasks/update/' . $task->id, $dataUpdate);
        $response->assertSessionHasErrors([
            'name',
            'content'
        ]);
    }


}
