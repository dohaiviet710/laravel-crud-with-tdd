@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    <a href="{{'tasks/create'}}" class="btn btn-primary" role="button" style="margin-bottom: 10px">
                        Create Tasks
                    </a>
                    <a href="{{'tasks/edit/3'}}" class="btn btn-primary" role="button" style="margin-bottom: 10px; margin-left: 10px">
                        Edit Tasks
                    </a>
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
