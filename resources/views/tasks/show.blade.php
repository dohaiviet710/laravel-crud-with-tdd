
@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row-justify-content-center">
            <div class="col-md-8">
                <h2>Show Task Has Id = {{$task->id}}</h2>
                <form>
                    <div class="card">
                        <div class="card-header">
                            <input type="text" readonly class="form-group" name="name" placeholder="Name ..." value="{{$task->name}}">
                        </div>
                        <div class="card-body">
                            <input type="text"  readonly class="form-group" name="content" placeholder="Content ..." value="{{$task->content}}">
                        </div>
                    </div>

                </form>

            </div>
        </div>

    </div>
@endsection
