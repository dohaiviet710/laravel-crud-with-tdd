
@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row-justify-content-center">
            <div class="col-md-8">
                <h2>Create Task</h2>
                <form action="{{route('tasks.update', $task->id)}}" method="POST">
                    @csrf
                    @method('put')
                    <div class="card">
                        <div class="card-header">
                            <input type="text" class="form-group" name="name" value="{{$task->name}}">
                            @error('name')
                            <span id="name-error" class="error text-danger" for= "name"
                                  style="..." >{{ $message }}

                            </span>
                            @enderror
                        </div>
                        <div class="card-body">
                            <input type="text" class="form-group" name="content" value="{{$task->content}}">
                            @error('content')
                            <span id="content-error" class="error text-danger" for= "content"
                                  style="..." >{{ $message }}
                            </span>
                            @enderror
                        </div>
                        <button class="btn btn-success">Submit</button>
                    </div>

                </form>

            </div>
        </div>

    </div>
@endsection
