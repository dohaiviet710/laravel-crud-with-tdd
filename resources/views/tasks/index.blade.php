
@extends('layouts.app')

@section('content')

    <div class="container">
        <a href="{{'tasks/create'}}" class="btn btn-primary" role="button" style="margin-bottom: 10px">
            Create New Tasks
        </a>

        <div class="row-justify-content-center">
            <table class="table table-striped">
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Content</th>
                    <th>Action</th>
                </tr>
                @foreach($tasks as $task)
                    <tr>
                        <th>{{$task->id}}</th>
                        <th>{{$task->name}}</th>
                        <th>{{$task->content}}</th>
                        <td>
                            <ul class="list-inline">
                                <li class="list-inline-item">
                                    <a href="{{route('tasks.edit', $task->id)}}" class="btn btn-primary" role="button" style="margin-bottom: 10px; margin-left: 10px">
                                    Edit Tasks
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <form action="{{ route('tasks.destroy', $task->id) }}" method="post" >
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-primary" type="submit">Delete</button>
                                    </form>
                                </li>
                                <li class="list-inline-item">
                                    <a href="{{route('tasks.show', $task->id)}}" class="btn btn-primary" role="button" style="margin-bottom: 10px; margin-left: 10px">
                                        Show Task
                                    </a>
                                </li>
                            </ul>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>

    </div>
@endsection
